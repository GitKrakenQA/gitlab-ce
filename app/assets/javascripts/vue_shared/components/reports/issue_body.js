import TestIssueBody from '~/reports/components/test_issue_body.vue';

export const components = {
  TestIssueBody,
};

export const componentNames = {
  TestIssueBody: TestIssueBody.name,
};
